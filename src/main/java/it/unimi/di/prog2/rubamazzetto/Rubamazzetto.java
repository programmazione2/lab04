package it.unimi.di.prog2.rubamazzetto;

import java.util.ArrayList;
import java.util.List;

public class Rubamazzetto {
  public static void main(String[] args) {
    List<Giocatore> partecipanti = new ArrayList<>();


    //TODO: creare i partecipanti componendo per ciascuno opportune strategie

    Partita p = new Partita(partecipanti);

    p.distribuisciMano(3);
    while (!p.isFinita()){

      System.out.println(p);
      for (Giocatore giocatore : partecipanti) {
        giocatore.turno();
      }
      p.distribuisciMano(1);
      System.out.println(p);

    }

  }
}
